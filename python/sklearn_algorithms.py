import pandas as pd
import numpy as np
import sklearn
from sklearn import svm
from sklearn.neighbors import KNeighborsClassifier
from sklearn import linear_model
from sklearn import metrics
import json


def create(X,y,Algorithm,testSize,ui):
    #split betweeen train and test data
    x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size = testSize)
   #get model
    args = {}
    if (Algorithm=="linear_regression"):
        method_to_call = getattr(linear_model,ui.regressionType.currentText())
        model = method_to_call()
        args = {"regressionType":ui.regressionType.currentText()}
    elif(Algorithm=="k_nearest_neighbours",testSize):
        model = KNeighborsClassifier(n_neighbors=ui.n_spin.value())
        args = {"n_neighbors":ui.n_spin.value()}
    elif(Algorithm=="support_vector_machine"):
        model = svm.SVC(kernel=ui.kernel.currentText())
        args = {"kernel":ui.kernel.currentText()}
    #apply non-default parameters if any
    params = ui.params_txt.toPlainText()
    params = json.loads(params)
    for key, value in params.items():
        model.key = value
        args[key] = value

    #fit model and work out accuracy according to metrics
    model.fit(x_train, y_train)
    y_predict = model.predict(x_test)
    Accs = []
    for m in ui.Metrics:
        method_to_call = getattr(metrics,m)
        try:
            acc = str(method_to_call(y_test,y_predict))
        except:
            acc = "NA"
        Accs.append(acc)
    return model,args,Accs

#def linear_regression_run(modName,x):
 #   print('Coefficient: \n', linear.coef_)
  #  print('Intercept: \n', linear.intercept_)

#    predictions = linear.predict(x_test)

#    for x in range(len(predictions)):
#        print(predictions[x], x_test[x], y_test[x])