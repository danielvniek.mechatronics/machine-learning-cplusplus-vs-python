# -*- coding: utf-8 -*-
"""
Created on Sat Sep 11 09:31:22 2021

@author: Daniel
"""

import sys
import create_ui
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5.uic import loadUi


class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        loadUi("ui_main.ui",self)
        self.create_btn.clicked.connect(self.newCreate)
        self.cwin = []

    def closeEvent(self,event):
        print("Closing")
    def newCreate(self):
        print("create")
        newC = create_ui.CreateWindow(self)  
        self.cwin.append(newC)
        newC.show()
    def updateModels(self):
        print("updating")

app = QApplication(sys.argv)
win = MainWindow()
win.show()
sys.exit(app.exec_())