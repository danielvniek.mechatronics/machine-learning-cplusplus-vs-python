# -*- coding: utf-8 -*-
"""
Created on Sat Sep 11 14:35:05 2021

@author: Daniel
"""
from PyQt5 import QtGui
from PyQt5.QtGui import QTextBlock
from PyQt5.uic import loadUi
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QApplication, QGridLayout, QHBoxLayout, QLabel, QMainWindow, QAction, QFileDialog, QPlainTextEdit, QScrollArea, QComboBox, QSpinBox, QTableView, QTableWidget, QTableWidgetItem, QTextEdit, QVBoxLayout
import pandas as pd
import sys
import sklearn_algorithms as skl
from sklearn import metrics
from sklearn import linear_model
from inspect import getmembers, isclass, isfunction
import pickle
import json
import traceback


def clearLayout(layout):
  while layout.count():
    child = layout.takeAt(0)
    if child.widget():
      child.widget().deleteLater()
    else:
        clearLayout(child)
      

        
class CreateWindow(QMainWindow):
    def algo_change(self,value):
        clearLayout(self.customAlgoArgs)
        if value=="linear_regression":
            h = QHBoxLayout()
            h.addWidget(QLabel("Regression Type:"))
            self.regressionType = QComboBox()
            RegTypes = []
            for m,o in getmembers(linear_model, isclass):
               RegTypes.append(m)
            self.regressionType.addItems(RegTypes)
            h.addWidget(self.regressionType)
            self.customAlgoArgs.addLayout(h)
        elif value=="k_nearest_neighbours":
            h = QHBoxLayout()
            h.addWidget(QLabel("Number of neighbours:"))
            self.n_spin = QSpinBox()
            self.n_spin.setMinimum(0)
            h.addWidget(self.n_spin)
            self.customAlgoArgs.addLayout(h)
        elif value=="support_vector_machine":
            h = QHBoxLayout()
            h.addWidget(QLabel("Kernel:"))
            self.kernel = QComboBox()
            self.kernel.addItems(["linear","poly","rbf","sigmoid"])
            h.addWidget(self.kernel)
            self.customAlgoArgs.addLayout(h)
    
    def __init__(self,main):
        super(CreateWindow, self).__init__()
        loadUi("create.ui",self)
        self.main = main
        self.data_btn.clicked.connect(self.choose_data)
        self.sep_cbox.addItems([",",";"])
        self.variables = QVBoxLayout()
        self.data_area_widget.setLayout(self.variables)
        self.data_area.setWidgetResizable(True)
        self.model_btn.clicked.connect(self.create_model)
        self.algorithms_cbox.addItems(["","linear_regression","k_nearest_neighbours","support_vector_machine"])
        self.algorithms_cbox.currentTextChanged.connect(self.algo_change)
        Mets = []
        for m,o in getmembers(metrics, isfunction):
            Mets.append(m)
        self.metrics_cbox.addItems(Mets)
        self.Metrics = []
        self.resultsRow = 1
        self.addMetric_btn.clicked.connect(self.addMetric)
        self.save_btn.clicked.connect(self.save_model)

    def addMetric(self):
        M = self.metrics_cbox.currentText()
        if self.Metrics.count(M)==0:
            self.Metrics.append(M)
            NewColNr = self.results_tab.columnCount()+1
            self.results_tab.setColumnCount(NewColNr)
            self.results_tab.setHorizontalHeaderItem(NewColNr-1,QTableWidgetItem(M))
            self.results_tab.resizeColumnsToContents()
    def closeEvent(self,event):
        global cwin
        self.main.cwin.remove(self)
    
    ##button functions
    def vartype_changed(self, value, variable):#this function is called when a variable type is changed to Input, Output or Ignore
        if value=="Input":
            self.inputs.append(variable)
            if self.outputs.count(variable)>0:
                self.outputs.remove(variable)

        elif value=="Output":
            self.outputs.append(variable)
            if self.inputs.count(variable)>0:
                self.inputs.remove(variable)
        else:
            if self.inputs.count(variable)>0:
                self.inputs.remove(variable)
            if self.outputs.count(variable)>0:
                self.outputs.remove(variable)
        print(self.inputs)
        print(self.outputs)
        txt = QPlainTextEdit()
        txt.toPlainText()

    def choose_data(self): #this function is called when the user clicks on the 'Choose data' button
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","CSV Files (*.csv)", options=options)
        if fileName:
            print(fileName)
            self.data = pd.read_csv(fileName, sep=self.sep_cbox.currentText())
            self.inputs = []
            self.outputs = []
            for id,col in enumerate(self.data.columns):
                h = QHBoxLayout()
                h.addWidget(QLabel(col))
                cb = QComboBox()
                cb.addItems(["Input","Output","Ignore"])
                if id==len(self.data.columns)-1:
                    cb.setCurrentIndex(1)
                    self.outputs.append(col)
                else:
                    self.inputs.append(col)
                cb.currentTextChanged.connect(lambda value,variable=col: self.vartype_changed(value,variable))
                h.addWidget(cb)
                self.variables.addLayout(h)

    def add_results(self,accuracies):
        global LastModel
        col=0
        NewRowNr = self.results_tab.rowCount()+1
        self.results_tab.setRowCount(NewRowNr)
        for i,a in enumerate(accuracies):
            M = self.Metrics[i]
            LastModel[M] = a
            self.results_tab.setItem(NewRowNr-1,col,QTableWidgetItem(a))
            col=col+1
        self.resultsRow = self.resultsRow+1

    def create_model(self):
        global LastModel
        if len(self.outputs)==1:
            algoName = self.algorithms_cbox.currentText()
            inputdata = self.data[self.inputs]
            outputdata = self.data[self.outputs]
            testSize = self.testSize_spin.value()/100
            if algoName!="":
                try:
                    model,args,accuracies=skl.create(inputdata,outputdata,algoName,testSize,self)
                    LastModel["algorithm"] = algoName
                    LastModel["inputs"] = self.inputs
                    LastModel["outputs"] = self.outputs
                    LastModel["model"] = model
                    LastModel["arguments"] = args
                    self.add_results(accuracies)
                except Exception as e:
                    print("Exception: "+str(e))
                    print(traceback.format_exc())

            else:
                print("No model selected")
        else:
            print("Error: only one output can be used")

    def save_model(self):
        global win
        name,ignore = QFileDialog.getSaveFileName(self, 'model1','',"Models (*.mlmod)")
        print(name)
        file = open(name,'wb')
        pickle.dump(LastModel["model"],file)
        file.close()
        try:
            allmodsFile = open("all_models.txt",)
            allModels = json.load(allmodsFile)
            allModels = json.loads(allModels)
            print(allModels)
        except:
            allModels = {}
            print("e")
            print(allModels)
        del LastModel["model"]
        print(LastModel)
        allModels[name] = LastModel
        with open('all_models.txt', 'w+') as fp:
            json.dump(json.dumps(allModels), fp)
        self.main.updateModels()



LastModel = {}

